/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.jellyfin.android.cyborg.ktx

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.view.View
import android.view.animation.DecelerateInterpolator

fun View.hide() {
    this.visibility = View.GONE
}

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.fadeIn(
    fadeDuration: Long = this.context.resources.getInteger(android.R.integer.config_shortAnimTime)
        .toLong(), listener: (() -> Unit)? = null
) {
    this.show()
    this.alpha = 0f
    val animator = this.animate().apply {
        interpolator = DecelerateInterpolator()
        duration = fadeDuration
        alpha(1f)
    }
    if (listener != null) {
        animator.setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                listener()
            }
        })
    }
}
