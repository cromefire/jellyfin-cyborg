/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.jellyfin.android.cyborg.helpers

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.os.Build
import android.provider.Settings
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import java.util.*

fun Context.deviceName(): String {
    val userConfName = Settings.Global.getString(contentResolver, "device_name")
    val m = Build.MANUFACTURER.toLowerCase(Locale.ROOT)
    val b = Build.BRAND.toLowerCase(Locale.ROOT)
    val d = Build.DEVICE.toLowerCase(Locale.ROOT)
    val p = Build.PRODUCT.toLowerCase(Locale.ROOT)
    val model = if (m in d || b in d) {
        Build.DEVICE
    } else if (m in p || b in p) {
        Build.PRODUCT
    } else {
        Build.MODEL
    }
    return "$userConfName ($model)"
}

val Context.securePrefs: SharedPreferences
    get() {
        val masterKeyAlias: String = MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)

        return EncryptedSharedPreferences.create(
            "cyborg-secret",
            masterKeyAlias,
            this,
            EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV,
            EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
        )
    }

val Context.preferences: SharedPreferences
    get() = getSharedPreferences("cyborg-prefs", MODE_PRIVATE)

fun SharedPreferences.getOrGenerate(key: String, generator: () -> String): String {
    var setting = getString(key, null)
    if (setting == null) {
        setting = generator()
        edit().putString(key, setting).apply()
    }
    return setting
}
