/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.jellyfin.android.cyborg.glue

import kotlinx.serialization.Serializable

@Serializable
data class ItemArgument(
    val Id: String
)

@Serializable
data class NativeError(
    val name: String?, val message: String?
)
