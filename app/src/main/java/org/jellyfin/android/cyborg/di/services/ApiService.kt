/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.jellyfin.android.cyborg.di.services

import android.content.Context
import org.jellyfin.android.cyborg.BuildConfig
import org.jellyfin.android.cyborg.helpers.deviceName
import org.jellyfin.android.cyborg.helpers.getOrGenerate
import org.jellyfin.android.cyborg.helpers.securePrefs
import org.jellyfin.api.ApiClient
import org.jellyfin.api.Platform
import org.jellyfin.api.PublicApiClient
import org.koin.core.KoinComponent
import org.koin.core.inject
import java.util.*

class ApiService : KoinComponent {
    private val context by inject<Context>()
    private val authService by inject<AuthService>()

    val client: ApiClient
        get() {
            val cache = clientCache
            if (cache != null && authService.requireCurrentApiCredentials().address == cache.serverAddress) {
                return cache
            }
            val new = generateClient()
            clientCache = new
            return new
        }

    private var clientCache: ApiClient? = null

    private fun generateClient(): ApiClient {
        val deviceId = context.securePrefs.getOrGenerate("deviceId") {
            UUID.randomUUID().toString()
        }

        val cred = authService.requireCurrentApiCredentials()

        return ApiClient(
            null, cred.address, Platform(
                "Project Cyborg", context.deviceName(), deviceId, BuildConfig.VERSION_NAME
            ), cred.token, cred.userId
        )
    }

    fun publicClient(address: String): PublicApiClient {
        val deviceId = context.securePrefs.getOrGenerate("deviceId") {
            UUID.randomUUID().toString()
        }

        return PublicApiClient(
            address, Platform(
                "Project Cyborg", context.deviceName(), deviceId, BuildConfig.VERSION_NAME
            )
        )
    }
}
