/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.jellyfin.android.cyborg.ui.login

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import org.jellyfin.android.cyborg.MainActivity
import org.jellyfin.android.cyborg.databinding.ActivityLoginBinding
import org.jellyfin.android.cyborg.di.services.ApiService
import org.jellyfin.android.cyborg.di.services.AuthService
import org.jellyfin.android.cyborg.ktx.*
import org.koin.android.ext.android.inject

class LoginActivity : AppCompatActivity() {
    private val authService by inject<AuthService>()
    private val apiService by inject<ApiService>()

    private lateinit var binding: ActivityLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        if (authService.loggedIn) {
            lifecycleScope.launch {
                delay(2000)  // Simulate some kind of network

                launchMain()
            }
            return
        }

        lifecycleScope.launch {
            delay(2000)  // Simulate some kind of network

            ui {
                binding.loading.hide()
                binding.loginScreen.fadeIn()
            }
        }

        binding.submit.setOnClickListener {
            lifecycleScope.launch {
                val url = binding.url.text.toString()
                val username = binding.username.text.toString()
                val password = binding.password.text.toString()

                val client = apiService.publicClient(url)

                val resp = client.authenticateUserByName(
                    username, password
                )

                authService.credentials = AuthService.ApiCredentials(
                    url, resp.user.id, resp.accessToken, resp.serverId
                )

                ui {
                    launchMain()
                }
            }
        }
    }

    private fun launchMain() {
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }
}
