/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.jellyfin.android.cyborg.di.services

import android.content.Context
import org.jellyfin.android.cyborg.helpers.securePrefs
import org.koin.core.KoinComponent
import org.koin.core.inject

class AuthService : KoinComponent {
    private val context by inject<Context>()

    var credentials: ApiCredentials?
        get() {
            val address = context.securePrefs.getString("address", null)
            val uid = context.securePrefs.getString("uid", null)
            val token = context.securePrefs.getString("token", null)
            val serverId = context.securePrefs.getString("server_id", null)
            if (address == null || uid == null || token == null || serverId == null) {
                return null
            }
            return ApiCredentials(address, uid, token, serverId)
        }
        set(value) {
            if (value == null) {
                context.securePrefs.edit().remove("address").remove("uid").remove("token").apply()
            } else {
                context.securePrefs.edit()
                    .putString("address", value.address)
                    .putString("uid", value.userId)
                    .putString("token", value.token)
                    .putString("server_id", value.serverId)
                    .apply()
            }
        }

    val loggedIn: Boolean
        get() = credentials != null

    fun requireCurrentApiCredentials(): ApiCredentials {
        return credentials ?: throw NullPointerException("ApiCredentials where null")
    }

    data class ApiCredentials(
        val address: String, val userId: String, val token: String, val serverId: String
    )
}